<?php

/**
 * User: someone else
 * Date: 18.03.15
 * Time: 14:47
 * To change this template use File | Settings | File Templates.
 *
 */
class Payment {
    const PREPAID_SUM = 1000;

    const TYPE_UNDERRIGHTING = 99; // причина траты денег андеррайтинг
    const TYPE_SUBSCRIPTION = 100; // причина траты денег абонентская плата
    const TYPE_LEAD_CONTRACT = 101; // причина траты денег покупка контракта
    const TYPE_PROTECTION_PRODUCT = 103; // причина траты денег защита клиента
    const TYPE_PREPAID = 300; // причина пополнения денег аванс
    const TYPE_PREPAID_VIRTUAL = 302; // причина траты денег аванс
    const TYPE_PREPAID_END = 301; // причина траты денег списание остатка аванс
    const SOURCE_BUY_BILLING_PRODUCT = 139; // причина траты денег продукт контакты

    const TYPE_MFI = 104; // причина пополнения
    const TYPE_NPF = 105; // причина пополнения НПФ
    const TYPE_CLIENTS_PROVIDER = 210;

    const TYPE_INCREASE_PROMO_CODE = 1;
    const TYPE_INCREASE_YANDEX = 0;
    const TYPE_INCREASE_QIWI = 14;
    const TYPE_INCREASE_PARTNERSHIP = 7;
    const TYPE_INCREASE_ALPHA = 13;
    const SOURCE_INCREASE_REVERSE_LOCK = 97;
    const TYPE_CHECKING_ACCOUNT = 4;
    const TYPE_QIWI_MANUAL = 6;

    const AVAILABLE_CUSTOMERS_HIDE = 0;
    const AVAILABLE_CUSTOMERS_SHOW = 1;

    const PAYMENT_GROUP_DEFAULT = 0;
    const PAYMENT_GROUP_INSTANT = 1;
    const PAYMENT_GROUP_WITH_IDENTIFY = 2;

    const TYPE_BROKER_LEAD = 107; // причина пополнения лиды от брокера
    const TYPE_PROMO_CODE_FIRE = 108; // причина пополнения лиды от брокера
    const TYPE_REVERT = 110;
    const TYPE_RETURN_PURCHASE = 130;
    const TYPE_RECALCULATION_PURCHASE = 202; // причина пополнения перерасчет проверки клиента
    const TYPE_RECALCULATION_BALANCE = 212; // причина перерасчет баланса
    const TYPE_RECEIVABLES = 213; // причина Дебиторская задолженность
    const TYPE_TRANSFER = 109;
    const TYPE_PLEDGE = 115; // причина пополнения ЗАЛОГИ
    const TYPE_BUG = 127;
    const TYPE_TRANSFER_FROM_BROKER = 222;
    const TYPE_TRANSFER_TO_BROKER = 223;

    const TYPE_LEARN_RESERVE = 200; // причина траты денег бронь обучения
    const TYPE_LEARN_BUY = 201; // причина траты денег покупка обучения

    const TYPE_RECALCULATION_NPF = 301;
    const TYPE_RECALCULATION_PLEDGE = 401;
    const TYPE_CONTRACT = 102; // способ пополнения - возврат по контракту

    const TYPE_CAPITOL = 33;

    /**
     * свойство для тестового вывода.
     */
    public $isTest = false;

    /**
     * константа яндекс кошелька
     * TODO: сменить на боевой, на!
     */
    //const YANDEX_ACCOUNT = '41001365546775';
    const YANDEX_ACCOUNT = '41001365546775';

    /**
     * константа минимально допустимой суммы для оплаты через способ требующий идентификация(формирование счета)
     */
    const MIN_AMOUNT_IDENTIFICATION = 490;

    /**
     * константа минимально допустимой суммы для оплаты через способ не требующий идентификация(платежные пошельки)
     */
    const MIN_AMOUNT_AUTOMATION = 10;

    /**
     * @deprecated
     */
    const TYPE_TRANSACTION_COMING = TransactionFinance::TYPE_INCREASE; // приход денег

    /**
     * @deprecated
     */
    const TYPE_TRANSACTION_CONSUMPTION = TransactionFinance::TYPE_DECREASE; // расход денег

    /**
     * @deprecated
     */
    const TYPE_TRANSACTION_TRANSFER = TransactionFinance::TYPE_TRANSFER;


    /**
     * массив мыльников для рассылки
     * @var array
     */
    private $emailList = [
        'buh' => 'buh@exbico.ru1',
        'client' => 'clients@exbico.ru'
    ];

    /**
     * массив групп способов оплаты
     * @var array
     */
    private $PaymentGroups = [1 => 'Мгновенные платежи', 2 => 'С идентификацией'];


    /**
     * ид Брокера заполняется при создании класса
     * @var mixed|null
     */
    private $broker = null; // к какоум брокеру относится транзакция.

    /**
     * метод ввода денег см ниже id_payment_method
     * указывается отдельно.
     * @var null
     */
    private $iIdPaymentMethod = null;

    /**
     * переменная хранит объект изера который совершает транзакцию
     * @var User
     */
    private $initiatorUser = null;

    /**
     * переменная хранит объект изера БАЛАНС которого затрагивает транзакция.
     * @var User
     */
    private $userReceived = null;

    /**
     * ид транзакции если она прошла успешно.
     */
    private $iIdTransaction = null;

    /**
     * список методов оплаты и их основные свойства
     * id_payment_method - ид оплаты из первой версии
     * description - описание в стиле аля админка
     * group принадлежность к группе платежей
     * sort порядок
     * available_customers используется ли клиентом(брокером для пополнения через интерфейс)
     * resource array массив для работы табов
     * @var array
     */

    static $paymentShow = [
        self::TYPE_INCREASE_YANDEX,
        self::TYPE_INCREASE_PROMO_CODE,
        3,
        self::TYPE_CHECKING_ACCOUNT,
        self::TYPE_QIWI_MANUAL,
        8,
        9,
        12,
        self::TYPE_INCREASE_ALPHA,
        self::TYPE_PROMO_CODE_FIRE,
        self::TYPE_RECALCULATION_BALANCE,
        self::SOURCE_LOCK,
        self::SOURCE_BUY_BILLING_PRODUCT,
        self::TYPE_PREPAID,
        self::TYPE_CAPITOL,
        self::TYPE_TRANSFER_TO_BROKER,
        self::TYPE_TRANSFER_FROM_BROKER,
    ];

    /*АХТУНГ что бы добавть способ оплаты НАДО:
    $aPaymentMethod добавить массив
    и указать УНИКАЛЬНЫЙ порядок сортировки(без разрывов) ДЛЯ ТЕХ способов оплаты у которых параметр
    'available_customers' => 1(показывать клиентам),
    */
    private $aPaymentMethod = [
        0  => [
            'id_payment_method'   => self::TYPE_INCREASE_YANDEX,
            'description_admin'   => 'Яндекс.Деньги',
            'description_user'    => 'Яндекс.Деньги',
            'group'               => self::PAYMENT_GROUP_INSTANT,
            'sort'                => 3,
            'available_customers' => self::AVAILABLE_CUSTOMERS_SHOW,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 0;}#',
                'image'    => '/images/ya_money.png',
            ],
        ],
        1  => [
            'id_payment_method'   => self::TYPE_INCREASE_PROMO_CODE,
            'description_admin'   => 'Промо-код',
            'description_user'    => 'Активация промо-кода',
            'group'               => self::PAYMENT_GROUP_INSTANT,
            'sort'                => 6,
            'available_customers' => self::AVAILABLE_CUSTOMERS_SHOW,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 1;}#',
            ],
        ],
        2  => [
            'id_payment_method'   => 3,
            'description_admin'   => 'Интернет-банк',
            'description_user'    => 'Интернет-банкинг',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
//            'group'               => self::PAYMENT_GROUP_WITH_IDENTIFY,
//            'sort'                => 0,
//            'available_customers' => self::AVAILABLE_CUSTOMERS_SHOW,
//            'resource'            => array(
//                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
//                'param'    => '#function(){return 3;}#',
//            ),
        ],
        3  => [
            'id_payment_method'   => self::TYPE_CHECKING_ACCOUNT,
            'description_admin'   => 'р/счет Альфа банк',
            'description_user'    => 'Расчетный счет компании',
            'group'               => self::PAYMENT_GROUP_WITH_IDENTIFY,
            'sort'                => 4,
            'available_customers' => self::AVAILABLE_CUSTOMERS_SHOW,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 4;}#',
            ],
        ],
        4  => [
            'id_payment_method'   => self::TYPE_QIWI_MANUAL,
            'description_admin'   => 'Qiwi (ручной)',
            'description_user'    => 'Сервис QIWI',
            'group'               => self::PAYMENT_GROUP_WITH_IDENTIFY,
            'sort'                => 7,
            'available_customers' => self::AVAILABLE_CUSTOMERS_SHOW,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 6;}#',
            ],

        ],
        5  => [
            'id_payment_method'   => self::TYPE_INCREASE_PARTNERSHIP,
            'description_admin'   => 'Партнерская программа',
            'description_user'    => 'Партнерская программа',
            'group'               => self::PAYMENT_GROUP_INSTANT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 7;}#',
            ],

        ],
        6  => [
            'id_payment_method'   => 8,
            'description_admin'   => 'Наличные',
            'description_user'    => 'Наличные',
            'group'               => self::PAYMENT_GROUP_INSTANT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 8;}#',
            ],
        ],
        7  => [
            'id_payment_method'   => 9,
            'description_admin'   => 'Яндекс.Деньги. В ручную',
            'description_user'    => 'Сервис Яндекс.Деньги',
            'group'               => self::PAYMENT_GROUP_WITH_IDENTIFY,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 9;}#',
            ],
        ],
        8  => [
            'id_payment_method'   => 11,
            'description_admin'   => 'По программе представителя',
            'description_user'    => 'По программе представителя',
            'group'               => self::PAYMENT_GROUP_INSTANT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 11;}#',
            ],

        ],
        9  => [
            'id_payment_method'   => 12,
            'description_admin'   => 'Сбербанк',
            'description_user'    => 'Карта Сбербанк',
            'group'               => self::PAYMENT_GROUP_WITH_IDENTIFY,
            'sort'                => 2,
            'available_customers' => self::AVAILABLE_CUSTOMERS_SHOW,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 12;}#',
                'image'    => '/images/sber.png',
            ],
        ],
        10 => [
            'id_payment_method'   => self::TYPE_INCREASE_ALPHA,
            'description_admin'   => 'VISA / MasterCard',
            'description_user'    => 'Карта VISA или MasterCard',
            'group'               => self::PAYMENT_GROUP_INSTANT,
            'sort'                => 5,
            'available_customers' => self::AVAILABLE_CUSTOMERS_SHOW,
            'resource'            => [
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 13;}#',
                'image'    => '/images/visa.jpg',
            ],

        ],
        /*11 => array(
            'id_payment_method'   => 14,
            'description_admin'   => 'Qiwi (авто)',
            'description_user'    => 'QIWI Кошелек',
            'group'               => self::PAYMENT_GROUP_INSTANT,
            'sort'                => 1,
            'available_customers' => self::AVAILABLE_CUSTOMERS_SHOW,
            'resource'            => array(
                'function' => '#DirectWrapper.PaymentDirectClassWrapper.selectPayment#',
                'param'    => '#function(){return 14;}#',
                'image'    => '/images/qiwi2.png',
            ),
        ),*/
        12 => [
            'id_payment_method'   => 102,
            'description_admin'   => 'Возврат средств по контракту',
            'description_user'    => 'Возврат средств по контракту',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        13 => [
            'id_payment_method'   => self::TYPE_MFI,
            'description_admin'   => 'Вознаграждение от МФО',
            'description_user'    => 'Вознаграждение от МФО',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        14 => [
            'id_payment_method'   => self::TYPE_BROKER_LEAD,
            'description_admin'   => 'Вознаграждение за поставку лидов',
            'description_user'    => 'Вознаграждение за поставку лидов',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        15 => [
            'id_payment_method'   => self::TYPE_NPF,
            'description_admin'   => 'Вознаграждение от НПФ',
            'description_user'    => 'Вознаграждение от НПФ',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        16 => [
            'id_payment_method'   => self::TYPE_PROMO_CODE_FIRE,
            'description_admin'   => 'Сгорание промо-кода',
            'description_user'    => 'Сгорание промо-кода',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        17 => [
            'id_payment_method'   => self::TYPE_RECALCULATION_PURCHASE,
            'description_admin'   => 'Перерасчет проверки клиента',
            'description_user'    => 'Перерасчет проверки клиента',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        18 => [
            'id_payment_method'   => 127,
            'description_admin'   => 'Перерасчет проверки клиента',
            'description_user'    => 'Перерасчет проверки клиента',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        19 => [
            'id_payment_method'   => self::TYPE_RECALCULATION_BALANCE,
            'description_admin'   => 'Перерасчет баланса',
            'description_user'    => 'Перерасчет баланса',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        20 => [
            'id_payment_method'   => self::TYPE_RECEIVABLES,
            'description_admin'   => 'Дебиторская задолженность',
            'description_user'    => 'Дебиторская задолженность',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        21 => [
            'id_payment_method'   => self::TYPE_PLEDGE,
            'description_admin'   => 'Залоги',
            'description_user'    => 'Залоги',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        22 => [
            'id_payment_method'   => self::TYPE_CLIENTS_PROVIDER,
            'description_admin'   => 'Вознаграждение за клиента в разделе "Предложение"',
            'description_user'    => 'Вознаграждение за клиента в разделе "Предложение"',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        23 => [
            'id_payment_method'   => self::TYPE_TRANSFER_FROM_BROKER,
            'description_admin'   => 'Перевод средств с баланса другого брокера',
            'description_user'    => 'Перевод средств с баланса другого брокера',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        24 => [
            'id_payment_method'   => self::TYPE_TRANSFER_TO_BROKER,
            'description_admin'   => 'Перевод средств на баланс другого брокера',
            'description_user'    => 'Перевод средств на баланс другого брокера',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        25 => [
            'id_payment_method'   => self::SOURCE_INCREASE_REVERSE_LOCK,
            'description_admin'   => 'Возврат с блока',
            'description_user'    => 'Возврат с блока',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        26 => [
            'id_payment_method'   => self::SOURCE_BUY_BILLING_PRODUCT,
            'description_admin'   => 'Покупка биллинг',
            'description_user'    => 'Покупка биллинг',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        27 => [
            'id_payment_method'   => self::SOURCE_LOCK,
            'description_admin'   => 'Блокировка денежных средств',
            'description_user'    => 'Блокировка денежных средств',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
        28 => [
            'id_payment_method'   => self::TYPE_CAPITOL,
            'description_admin'   => 'Вознаграждение Капитоль',
            'description_user'    => 'Вознаграждение Капитоль',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],

        29 => [
            'id_payment_method'   => self::TYPE_PREPAID,
            'description_admin'   => 'Аванс',
            'description_user'    => 'Аванс',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],



        30 => [
            'id_payment_method'   => self::TYPE_PREPAID_END,
            'description_admin'   => 'Списание остатка по авансу',
            'description_user'    => 'Списание остатка по авансу',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],

        31 => [
            'id_payment_method'   => self::TYPE_PREPAID_VIRTUAL,
            'description_admin'   => 'Аванс',
            'description_user'    => 'Аванс',
            'group'               => self::PAYMENT_GROUP_DEFAULT,
            'sort'                => 0,
            'available_customers' => self::AVAILABLE_CUSTOMERS_HIDE,
            'resource'            => [],
        ],
    ];

    /**
     * пиздец
     * (
     * а на самом деле короче видишь переменную $aPaymentMethod?
     * так вот в $aGetKeyBySource key это значение id_payment_method из $aPaymentMethod
     * а value в $aGetKeyBySource это key $aPaymentMethod. вот.
     * )
     * @var array
     */
    public $aGetKeyBySource = array(
        0  => 0,
        1  => 1,
        3  => 2,
        4  => 3,
        6  => 4,
        7  => 5,
        8  => 6,
        9  => 7,
        11 => 8,
        12 => 9,
        13 => 10,
        14 => 11,
        102 => 12,
        104 => 13,
        107 => 14,
        105 => 15,
        108 => 16,
        115 => 21,
        202 => 17,
        127 => 18,
        212 => 19,
        213 => 20,
        self::TYPE_CLIENTS_PROVIDER => 22,
        self::TYPE_TRANSFER_FROM_BROKER => 23,
        self::TYPE_TRANSFER_TO_BROKER => 24,
        self::SOURCE_INCREASE_REVERSE_LOCK => 25,
        self::SOURCE_BUY_BILLING_PRODUCT => 26,
        self::SOURCE_LOCK => 27,
        self::TYPE_CAPITOL => 28,
        self::TYPE_PREPAID => 29,
        self::TYPE_PREPAID_END => 30,
        self::TYPE_PREPAID_VIRTUAL => 31,
    );

    public function getMethodNameBySource($source){
        if(isset($this->aGetKeyBySource[$source])){
            return $this->aPaymentMethod[$this->aGetKeyBySource[$source]]['description_admin'];
        }else{
            throw new CException('source $source not found');
        }
    }

    /**
     * список виджетов для отображения ключ = id_payment_method
     * @var array
     */
    private $aPaymentMethodWidget = [
//      яндекс автомат
        0  => [
            'widgetTemplate' => ['yandexAuto',],
        ],
//        промо код
        1  => [
            'widgetTemplate' => ['promoCode',],
        ],
//        интернет банкинг
        3  => [
            'widgetTemplate' => ['InternetBanking',],
        ],
//      Банковский счет
        4  => [
            'widgetTemplate' => ['bankAccount',],
        ],
//        сервис Qiwi в ручную.
        6  => [
            'widgetTemplate' => ['qiwi',],
        ],
//        Партнерская программа
        7  => [
            'widgetTemplate' => ['test', 'test2'],
        ],
//        Оплата наличкой
        8  => [
            'widgetTemplate' => ['test', 'test2'],
        ],
//        яндекс деньги в ручную
        9  => [
            'widgetTemplate' => ['yandexFile'],
        ],
//        Программа представителя.
        11 => [
            'widgetTemplate' => ['test', 'test2'],
        ],
//        Оплата картой сбербанка
        12 => [
            'widgetTemplate' => ['cardSberbank',],
        ],
//        Виза или мастер карт
        13 => [
            'widgetTemplate' => ['visaMaster',],
        ],
//        qiwi автомат.
        14 => [
            'widgetTemplate' => ['QiwiAuto',],
        ],
    ];


    /**
     * массив связи для выяснения какой на самом деле вызывается метод оплаты.
     * поскольку в автомат. системах оплаты дублируется интерфейс методов с подтверждением
     * пришлось сделать связь между автоматическими способами и их не автоматичискими аналогами.
     * ключ это реальный ид способа значение это вид не автоматического
     * у кого нет не автоматического значние они равны
     * @var array
     */
    public $aIdentifierPaymentMethod = array(
        0  => 9,
        1  => 1,
        3  => 3,
        4  => 4,
        6  => 6,
        7  => 7,
        8  => 8,
        9  => 9,
        11 => 11,
        12 => 12,
        13 => 13,
        14 => 6,
    );

    public $aDescriptionPaymentMethod;

    /**
     * @var IBalanceManager $balanceManager
     */
    protected $balanceManager;

    /**
     * @param null $initiatorUserId
     * @param null $idUserRecipient
     * @throws CException
     * @throws Exception
     */
    public function  __construct($initiatorUserId = null,$idUserRecipient = null) {
        if (isset($initiatorUserId)) {
            $initiatorUser = User::model()->findByPk($initiatorUserId);
            if ($initiatorUser && $initiatorUser->isClient()) {
                $this->initiatorUser = $initiatorUser;
                $this->broker = empty($this->initiatorUser->broker) ? $this->initiatorUser->id : $this->initiatorUser->broker;
                $this->whoPutMoney($idUserRecipient);
                $this->balanceManager = new BalanceManager(new FinanceUser($this->userReceived->id));
            } else {
                throw new CException("bad initiatorUserId {$initiatorUserId}");
            }
        }
    }

    /**
     * @param $property
     * @return mixed
     */
    public function __get($property) {
        return property_exists($this, $property) ? $this->$property : null;
    }



    /**
     * устанавливает значение iIdPaymentMethod
     * @param $idPaymentMethod
     */
    public function setIdPaymentMethod($idPaymentMethod) {
        $this->iIdPaymentMethod = $idPaymentMethod;
    }

    /**
     * возвращает ид транзакции если она произошла.
     * если нет, то вернет null
     */
    public function getIdTransaction() {
        return $this->iIdTransaction;
    }

    /* функция определяет какому Ид надо положить денег изходя  из прав  пользователя.
        т.е если пользователь = сотрудник и у него нет собственного баланса тогда мы кладем денег брокеру
        иначе мы кладем денег сотруднику.
    */

    /**
     * функция определяет кому надо перечислить деньги, сотруднику или брокеру.
     * @param $idUserRecipient
     * @throws CException
     */
    private function whoPutMoney($idUserRecipient) {
        if($this->initiatorUser->isClient()) {
            if (isset($idUserRecipient)) {
                if ($this->initiatorUser->isBroker()) {
                    $oUserReceived = User::model()->findByAttributes([
                        'id'     => $idUserRecipient,
                        'broker' => $this->initiatorUser->id
                    ]);
                } else {
                    $oUserReceived = User::model()->findByPk($this->broker);
                }
            } else {
                if ($this->initiatorUser->isEmployer() && Yii::app()->authManager->checkAccess(User::TASK_PERSONAL_BALANCE, $this->initiatorUser->id)) {
                    $oUserReceived = $this->initiatorUser;
                } else {
                    $oUserReceived = User::model()->findByPk($this->broker);
                }
            }
            if (isset($oUserReceived)) {
                $this->userReceived = $oUserReceived;
            } else {
                throw new CException('пользоветель не найден');
            }
        } else {
            throw new CException('bad user type');
        }
    }

    /**
     * функция добавляет уведомление в таблицу с которой работает крон (рассыльщик.)
     * @param $iIdFile
     * @param $iIdPaymentIdentification
     * @return bool
     */
    private function addNoticeIdentifierPayment($iIdFile, $iIdPaymentIdentification) {
        $aArray = array(
            'date'          => Helper::getNow(),
            'paymentMethod' => $this->aPaymentMethod[$this->aGetKeyBySource[$iIdPaymentIdentification]]['description_admin'],
        );
        if ($this->broker == $this->initiatorUser->id) {
            $aArray['message'] = "Новое подтверждение платежа для " . $this->initiatorUser->getCaption($bAddPrefix = true, $noCompany = false, $middleName = true) . '[' . $this->initiatorUser->login . ']';
            $aArray['phone'] = $this->initiatorUser->phone;
        } // если кладут брокеру тогда дописываем что кладет сотрудник
        elseif ($this->broker != $this->initiatorUser->id && $this->broker == $this->userReceived->id) {
            $aArray['message'] = "Новое подтверждение платежа для " . $this->userReceived->getCaption($bAddPrefix = true, $noCompany = false, $middleName = true) . '[' . $this->userReceived->login . ']' . ' от ' . $this->initiatorUser->getCaption($bAddPrefix = true, $noCompany = false, $middleName = true) . '[' . $this->initiatorUser->login . ']';
            $aArray['phone'] = $this->initiatorUser->phone;

        } elseif ($this->broker != $this->initiatorUser->id && $this->broker != $this->userReceived->id) {
            $aArray['message'] = "Новое подтверждение платежа для Брокера Б-" . $this->broker . ' от сотрудника' . $this->initiatorUser->getCaption($bAddPrefix = true, $noCompany = false, $middleName = true) . '[' . $this->initiatorUser->login . ']' . ' на счет сотрудника';
            $aArray['phone'] = $this->initiatorUser->phone;
        }
        foreach($this->emailList as $sEmail){
            $model = new MainNotification;
            $model->composeEmail(MainNotification::TPL_BALANCE_IDENTIFIER_PAYMENT, $sEmail, $aArray, 0, false, $iIdFile);
        }
        return true;
    }


    /**
     * функция возвращает ИД метода с идентификацией если такой ид оплаты существует инче false.
     * @return bool
     */
    public function getIdentifierPaymentMethod() {
        if (isset($this->aIdentifierPaymentMethod[$this->iIdPaymentMethod])) {
            return $this->aIdentifierPaymentMethod[$this->iIdPaymentMethod];
        } else {
            return false;
        }
    }

    /**
     * функция возвращает массив виджета для конкретного ИД способо оплаты, если такого нет вернет массив для всех способов оплаты.
     * @param null $id_payment_method
     * @return array
     */
    public function getPaymentMethodWidget($id_payment_method = null) {
        if (isset($this->aPaymentMethodWidget[$id_payment_method])) {
            return $this->aPaymentMethodWidget[$id_payment_method]['widgetTemplate'];
        } else {
            return $this->aPaymentMethodWidget;
        }
    }

    /**
     * возвращает инфо о  методе оплаты по ключу.
     * если ключа нет тогда все.
     * @param null $iKeyPaymentMethod
     * @return array
     */
    public function getPaymentMethod($iKeyPaymentMethod = null) {
        if (isset($this->aPaymentMethod[$iKeyPaymentMethod])) {
            return $this->aPaymentMethod[$iKeyPaymentMethod];
        } else {
            return $this->aPaymentMethod;
        }
    }

    static function init() {
        return new self();
    }

    /**
     * возвращает инфо о Группе оплаты
     * если ид нет тогда все.
     * @param null $iIdGroup
     * @return array
     */
    public function getGroup($iIdGroup = null) {
        if (isset($this->PaymentGroups[$iIdGroup])) {
            return $this->PaymentGroups[$iIdGroup];
        } else {
            return $this->PaymentGroups;
        }
    }

    /**
     * @param $fileId
     * @return bool
     */
    public function addPaymentIdentification($fileId) {
        // тут заносим таблицу main_payment_identification данные о прибытии файла.
        $oModelPaymentIdentify = new PaymentIdentification();
        $oModelPaymentIdentify->id_user = $this->userReceived->id; // кому кладем денег
        $oModelPaymentIdentify->id_user_created = $this->initiatorUser->id; // кто кладет деньги.
        $oModelPaymentIdentify->create_data = Helper::getNow();
        $oModelPaymentIdentify->id_file = $fileId; //ид файла
        $oModelPaymentIdentify->id_payment_method = $this->iIdPaymentMethod; //ид способа оплаты.

        if ($oModelPaymentIdentify->validate()) {
            $oModelPaymentIdentify->save();
//          заносим письма в таблицу для отправки.
            return $this->addNoticeIdentifierPayment($fileId, $this->iIdPaymentMethod);
        } else {
            return false;
        }
    }


    /**
     * функция принимает массив
     * отвечает за проведение пополняющих транзакций.
     * @param PaymentParams $params
     * @throws CException
     * @return bool
     */
    public function addTransactionComing(PaymentParams $params) {
        try {
            $params->setTypeTransaction(TransactionFinance::TYPE_INCREASE);
            if ($this->addTransaction($params)) {
                $this->userReceived->balance_finance = $this->roundResult($this->userReceived->balance_finance + $params->deltaRealSum);
                $this->userReceived->balance_points = $this->roundResult($this->userReceived->balance_points + $params->deltaVirtualSum);
                $this->balanceManager->increase($params->deltaRealSum,$params->deltaVirtualSum);
                $this->sendIncomeNotification($params);
                if ($this->userReceived->save()) {
                    $result = true;
                    $this->userReceived->addNotificationBalanceIncrease($params->getDelta());
                } else {
                    throw new CException('ошибка при пополнении баланса (1) $this->userReceived->save() не отработал');
                }
            } else {
                throw new CException('ошибка при пополнении баланса (2) неудалось добавить транзакцию');
            }
        } catch (CException $e) {
            Yii::log($e->getMessage(),CLogger::LEVEL_ERROR,'app.finance.addTransactionComing');
            throw $e;
        }
        return $result;
    }

    protected function sendIncomeNotification(PaymentParams $params) {
        if (!empty($params->transactionComment)) {
            $oNotification = new MainNotification();
            $oNotification->composeEmail(MainNotification::TPL_BALANCE_RECHARGE_FOR_MANAGER, $this->emailList['buh'], [
                'paymentMethodName' => $this->aPaymentMethod[$this->aGetKeyBySource[$params->paymentReason]]['description_admin'],
                'userName'          => $this->userReceived->getCaption($bAddPrefix = true, $noCompany = false, $middleName = true),
                'amount'            => $params->getDelta()
            ]);
        }
    }

    protected function roundResult($result) {
        return round($result,2);
    }


    /**
     * функция принимает массив
     * отвечает за проведение списывающих транзакций.
     * @param PaymentParams $params
     * @param bool $force
     * @param bool $prepaid
     * @throws CException
     * @return bool
     */
    public function addTransactionConsumption(PaymentParams $params, $force = false, $prepaid = false)
    {
        try {
            $params->setTypeTransaction(TransactionFinance::TYPE_DECREASE);
            if ($prepaid && !in_array((int)$params->paymentReason, [ //todo understand logic and may be remove this code
                    Payment::TYPE_UNDERRIGHTING,
                    Payment::SOURCE_INCREASE_REVERSE_LOCK,
                    Payment::SOURCE_BUY_BILLING_PRODUCT,
                    Payment::SOURCE_LOCK,
                    Payment::TYPE_RECALCULATION_PURCHASE,
                    Payment::TYPE_LEAD_CONTRACT
                ], true)
            ) {
                $residue = $this->userReceived->getBalanceWithoutVirtual() - $params->getDelta();
            } else {
                $residue = $this->userReceived->getBalance() - $params->getDelta();
            }
            $correctOperation = ($residue >= 0);
            if ($correctOperation || $force) {
                if($prepaid && in_array($params->paymentReason, [Payment::TYPE_UNDERRIGHTING, Payment::SOURCE_INCREASE_REVERSE_LOCK, Payment::SOURCE_BUY_BILLING_PRODUCT, Payment::SOURCE_LOCK, Payment::TYPE_RECALCULATION_PURCHASE])) {
                    $params->reCalculateDelta($this->userReceived->balance_points);
                } else if ($prepaid == false) {
                    $params->reCalculateDelta($this->userReceived->balance_points);
                }
                $this->addTransaction($params);
                $this->userReceived->balance_finance = $this->roundResult($this->userReceived->balance_finance - $params->deltaRealSum);
                $this->userReceived->balance_points = $this->roundResult($this->userReceived->balance_points - $params->deltaVirtualSum);
                if (!$this->balanceManager->changeBalance(-$params->getDelta(),$force)) {
                    Yii::log("Неудачная попытка списания средств для пользователя {$this->initiatorUser->id} по транзакции {$this->iIdTransaction}",
                        CLogger::LEVEL_ERROR, 'app.finance.new.addTransactionConsumption');
                }
                if ($this->userReceived->save()) {
                    $this->userReceived->CheckNotification(); //TODO возможно перенсти отсюда
                    $result = true;
                    if (!$correctOperation && $force) {
                        Yii::log("Внимание! Произошло списание в минус по транзакции {$this->iIdTransaction}! ({$residue}/{$params->getDelta()})",
                            CLogger::LEVEL_WARNING, 'app.finance.addTransactionConsumption');
                    }
                } else {
                    throw new \common\models\exceptions\CriticalException("ошибка при снятии средств с баланса");
                }
            } else {
                if($prepaid && !in_array($params->paymentReason, [Payment::TYPE_UNDERRIGHTING, Payment::SOURCE_INCREASE_REVERSE_LOCK, Payment::SOURCE_BUY_BILLING_PRODUCT, Payment::SOURCE_LOCK, Payment::TYPE_RECALCULATION_PURCHASE])) {
                    throw new \common\models\exceptions\SoftException("Недостаточно средств {$this->userReceived->getBalanceWithoutVirtual()}/{$params->getDelta()}");
                } else {
                    throw new \common\models\exceptions\SoftException("Недостаточно средств {$this->userReceived->getBalance()}/{$params->getDelta()}");
                }
            }
        } catch (\common\models\exceptions\CriticalException $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'app.finance.addTransactionConsumption');
            throw $e;
        }
        return $result;
    }

    /**
     * todo
     * @param PaymentParams $params
     * @param bool $force
     * @return bool
     * @throws \common\models\exceptions\CriticalException
     * @throws \common\models\exceptions\SoftException
     */
    public function addTransactionConsumptionPrepaid(PaymentParams $params,$force = false) {
        return $this->addTransactionConsumption($params, $force, true);
    }

    /**
     * функця перевода денег со счета брокера сотруднику и наоборот если класс создан от Ид сотрудника
     * не принимает орицательных значений
     * @param PaymentParams $params
     * @return bool
     * @throws CException
     * @throws CException
     */
    public function addTransactionTransfer(PaymentParams $params) {
        try {
            $params->paymentReason = self::TYPE_TRANSFER;
            $params->setTypeTransaction(TransactionFinance::TYPE_TRANSFER);
            if (($params->getDelta() >= 0 && $this->initiatorUser->getBalance() >= $params->getDelta())) {
                $params->reCalculateDelta($this->initiatorUser->balance_points);
                if ($this->addTransaction($params)) {
                    $balanceManagerForSourceUser = new BalanceManager(new FinanceUser($this->initiatorUser->id));
                    $this->initiatorUser->balance_finance = $this->roundResult($this->initiatorUser->balance_finance - $params->deltaRealSum);
                    $this->initiatorUser->balance_points = $this->roundResult($this->initiatorUser->balance_points - $params->deltaVirtualSum);
                    if(!$balanceManagerForSourceUser->changeBalance(-$params->getDelta())) {
                        Yii::log("Неудачная попытка списания средств для пользователя {$this->initiatorUser->id} по транзакции {$this->iIdTransaction}",
                            CLogger::LEVEL_ERROR, 'app.finance.new.addTransactionTransfer');
                    }
                    if ($this->initiatorUser->save()) {
                        $this->initiatorUser->CheckNotification();
                        $this->userReceived->balance_finance = $this->roundResult($this->userReceived->balance_finance + $params->deltaRealSum);
                        $this->userReceived->balance_points = $this->roundResult($this->userReceived->balance_points + $params->deltaVirtualSum);
                        $this->balanceManager->increase($params->deltaRealSum,$params->deltaVirtualSum);
                        if ($this->userReceived->save()) {
                            $result = true;
                            $this->userReceived->addNotificationBalanceIncrease(($params->getDelta()));
                        } else {
                            throw new CException("неудалось начислить деньги на счет");
                        }
                    } else {
                        throw new CException("неудалось снять деньги со счета");
                    }
                } else {
                    throw new CException("ошибка при переводе средств");
                }
            } else {
                throw new CException("Недостаточно денег для перевода! ({$this->initiatorUser->getBalance()}/{$params->getDelta()}");
            }
        } catch (CException $e) {
            $result = false;
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'app.finance.addTransactionConsumption');
        }
        return $result;
    }

    public function changeRealBalance(PaymentParams $params, $force = false) {
        if ($params->getDelta() > 0) {
            $result = $this->addTransactionComing($params);
        } else {
            $params->deltaRealSum = abs($params->getDelta());
            $params->deltaVirtualSum = 0;
            $result = $this->addTransactionConsumption($params,$force);
        }
        return $result;
    }


    /**
     * метод добавляет транзакцию в таблицу транзакций.
     * @param PaymentParams $params
     * @return bool
     * @throws \common\models\exceptions\CriticalException
     */
    private function addTransaction(PaymentParams $params) {
        $transactionFinance = new TransactionFinance();
        $transactionFinance->broker = $this->broker;
        $transactionFinance->id_user_pay = $this->initiatorUser->id;
        $transactionFinance->id_user_received = $this->userReceived->id;
        $transactionFinance->period = Period::get();
        $transactionFinance->source = $params->paymentReason;
        $transactionFinance->value = $params->deltaRealSum; // $this->iCountMoney
        $transactionFinance->virtual = $params->deltaVirtualSum; //  $this->iCountVirtualMoney;
        $transactionFinance->type = $params->getTypeTransaction();
        $transactionFinance->payment_transaction = $params->transactionComment;
        $transactionFinance->operator = $params->idOperator;
        $transactionFinance->disabled = $params->getReverted();
        if ($transactionFinance->save()) {
            $this->iIdTransaction = $transactionFinance->id;
            return true;
        } else {
            throw new \common\models\exceptions\CriticalException("TransactionFinance did not saved. errors ".json_encode($transactionFinance->getErrors()));
        }

    }

    /**
     * функция принимает массив
     * отвечает за отмену списывающей транзакции и пересчет баланса
     * @param TransactionFinance $revertedTransaction
     * @param bool $force
     * @throws CException
     * @return bool
     */
    public function revertTransaction(TransactionFinance $revertedTransaction, $force = false) {
        try {
            if ($this->userReceived->id == $revertedTransaction->id_user_received) {
                if (!$revertedTransaction->isReverted()) {
                    $params = new PaymentParams([
                        'deltaRealSum'    => $revertedTransaction->value,
                        'deltaVirtualSum' => $revertedTransaction->virtual
                    ]);
                    $params->paymentReason = self::TYPE_REVERT;
                    $params->setReverted(TransactionFinance::IS_REVERTED);
                    $revertedTransaction->disabled = TransactionFinance::IS_REVERTED;

                    switch ($revertedTransaction->type) {
                        case TransactionFinance::TYPE_INCREASE: $result = $this->addTransactionConsumption($params, $force, $this->userReceived->isMinTariffsCozPrepaid());break;
                        case TransactionFinance::TYPE_DECREASE: $result = $this->addTransactionComing($params);break;
                        default : throw new CException("type ({$revertedTransaction->type}) does not supported revert");
                    }
                    if(!$revertedTransaction->save()) {
                        throw new CException("transaction is not saved. errors " . json_encode($revertedTransaction->getErrors()));
                    }
                } else {
                    throw new CException("transaction is already reverted");
                }
            } else {
                throw new CException("bad user =(");
            }
        }  catch (CException $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'app.finance.addTransactionConsumption');
            throw $e;
        }
        return $result;
    }

    /**
     * функция генерирует label для ya.money который придет в месте с ответом.
     * @return string
     */
    public function generateResponseToYaMoney() {
        return json_encode(array('who' => $this->initiatorUser->id));
    }

    private function initDescriptionPaymentMethod() {
        if (empty($this->aDescriptionPaymentMethod)) {
            foreach($this->aPaymentMethod as $aMethod){
                $this->aDescriptionPaymentMethod[$aMethod['id_payment_method']] = array(
                    'description_user'  => $aMethod['description_user'],
                    'description_admin' => $aMethod['description_user'],
                    'id_payment_method' => $aMethod['id_payment_method'],
                );
            }
        }
        return $this->aDescriptionPaymentMethod;
    }

    public function getDescriptionTransaction($oTransaction) {
        $message = '';
        $aDescriptionPaymentMethod = self::initDescriptionPaymentMethod();
        if ($oTransaction->type == TransactionFinance::TYPE_INCREASE) {

            if(isset($aDescriptionPaymentMethod[$oTransaction->source])) {
                $message = $aDescriptionPaymentMethod[$oTransaction->source]['description_user'];
            }else{
                $message = '';
            }

            if ($oTransaction->id_user_pay != $oTransaction->id_user_received) {
                $message .= 'от ' . $oTransaction->UserPay->getCaption(true);
            }

            // если контракт то узнаем id контракта
            if(isset($aDescriptionPaymentMethod[$oTransaction->source])) {
                if ($aDescriptionPaymentMethod[$oTransaction->source]['id_payment_method'] == self::TYPE_CONTRACT) {
                    $contract = LeadContract::model()->findByAttributes(array(
                        'id_transaction_close' => $oTransaction->id
                    ));
                    if ($contract) {
                        $message .= ' №' . $contract->id;
                    }
                }
            }

        }

        if ($oTransaction->type == TransactionFinance::TYPE_DECREASE && in_array($oTransaction->source, self::$paymentShow)) {

            if(isset($aDescriptionPaymentMethod[$oTransaction->source])) {
                $message = $aDescriptionPaymentMethod[$oTransaction->source]['description_user'];
            }else{
                $message = '';
            }

            if ($oTransaction->id_user_pay != $oTransaction->id_user_received) {
                $message .= 'от ' . $oTransaction->UserPay->getCaption(true);
            }

            // если контракт то узнаем id контракта
            if(isset($aDescriptionPaymentMethod[$oTransaction->source])) {
                if ($aDescriptionPaymentMethod[$oTransaction->source]['id_payment_method'] == self::TYPE_CONTRACT) {
                    $contract = LeadContract::model()->findByAttributes(array(
                        'id_transaction_close' => $oTransaction->id
                    ));
                    if ($contract) {
                        $message .= ' №' . $contract->id;
                    }
                }
            }

        }

        if ($oTransaction->type == TransactionFinance::TYPE_TRANSFER) {
            $message = 'перевод со счета ' . $oTransaction->UserPay->getCaption(true) . ' на счет ' . $oTransaction->UserReceived->getCaption(true);
        }
        return $message;
    }
}

